from __future__ import absolute_import
import os
import random
from collections import defaultdict
import sys
import time
import re

from lr_model import LRTagger, read_data

#word2vec disabled for now
#https://github.com/RichardKelley/intro-data-science/blob/master/week-09/gensim-pos.ipynb
#from gensim.models import Word2Vec

def printTime(totalTime, str):
    m, s = divmod(totalTime, 60)
    h, m = divmod(m, 60)
    print "%s = %.0f[s]. [h:m:s] = %d:%02d:%02d" % (str, totalTime, h, m, s)

def main():
    import optparse
    cmdlineParser = optparse.OptionParser("%prog [options]")
    cmdlineParser.add_option("-t", "--train", type="string", dest="trainFile",
                             help="file for training", default="")
    cmdlineParser.add_option("", "--test", type="string", dest="testFile",
                             help="file for testing", default="")
    cmdlineParser.add_option("-f", "--testformat", type="int", dest="testFormat",
                             help="0: word tag per line. 1: sentence per line (default: 1)", default="1")
    cmdlineParser.add_option("-s", "--save", type="string", dest="saveModelFile",
                             help="save model to file", default="")    
    cmdlineParser.add_option("-l", "--load", type="string", dest="loadModelFile",
                             help="load model from file", default="")                                                              
    cmdlineParser.add_option("-w", "--wordvectors", type="string", dest="wordvecFile",
                             help="wordvec file", default="")                                                              
    cmdlineParser.add_option("-n", "--norm", action="store_true", dest="donormalize",
                             help="perform some normalization of words (False)", default=False)                             
    cmdlineParser.add_option("-i", "--iter", type="int", dest="num_iters",
                             help="number of training iterations (default: 5)", metavar="N", default=5)
    cmdlineParser.add_option("-j", "--threads", type="int", dest="num_threads",
                             help="number of threads for CV (default: 4)", metavar="N", default=4)
    cmdlineParser.add_option("-v", "--verbose", type="int", dest="verbose",
                             help="verbosity level (0 verbosity, only a sumamry is printer)", default=0)
    (options, args) = cmdlineParser.parse_args()
    
    global verbose
    verbose         = options.verbose
    
    start = time.clock()
    
    word_model=None
    if options.wordvecFile:
        print "loading wordvec..."
        bin=False
        if re.search('\.bin',options.wordvecFile):
            bin=True
        word_model = Word2Vec.load_word2vec_format(options.wordvecFile, binary=bin)
    model = LRTagger(donormalize=options.donormalize, word_model=word_model, 
                num_threads=options.num_threads)
    
    if options.loadModelFile:
        print "loading model file from", options.loadModelFile
        model.load(options.loadModelFile)
    
    if options.trainFile:
        print "training model from:", options.trainFile
        model.train(options.trainFile, num_iters=options.num_iters)
        if options.saveModelFile:
            model.save(options.saveModelFile)
    
    correct = 0
    totalWords = 0
    if options.testFile:
        print "tagging and scoring testfile: ", options.testFile
        tagStart = time.clock()
        totalWords = 0
        if options.testFormat==0:
            sentences = read_data(options.testFile, options.donormalize)
            for words, goldTags in sentences:
                totalWords += len(words)
                if words:
                    predictedTags = model.tag(words)
                    match = [i for i, j in zip(predictedTags, goldTags) if i == j]
                    correct += len(match)
                    (words, goldTags) = ([],[])
            print "test accuracy: %d/%d = %.2f" %(correct, totalWords, 100.0*correct/totalWords)
        else:
            with open(options.testFile) as f:
                for line in f:
                    words = line.split()
                    totalWords += len(words)
                    predictedTags = model.tag(words)
                    print "{",
                    for i in range(len(words)):
                        print '"%s":"%s"' %(words[i], predictedTags[i]),
                        if i<len(words)-1:
                            print ",", 
                    print "}"
        tagEnd = time.clock()
        printTime(tagEnd-tagStart, "Tagging time")
        print "tagging speed = %d/%d = %.0f[w/s]" % (totalWords, tagEnd-tagStart, totalWords/(tagEnd-tagStart))

    
    end = time.clock()
    printTime(end-start, "Total time")
    
if __name__ == "__main__":
    main()
    sys.exit(0)
    
