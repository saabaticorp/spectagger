
use strict;

#words that most likely have this tag
my %xWords = (
    "with"  => "func",
    "or"    => "func",
    "for"   => "func",
    "or"    => "func",
    "of"    => "func",
    "in"    => "func",
    "by"    => "func",
    "size"  => "x",
    "new"   => "x",
    "&amp;" => "punc",
);

#some stats
my ($numItems, $itemsKept) = (0,0);

#build hash of word-tag-frequency
open(IN,"<$ARGV[0]");
my %wordtagfreq = ();

print STDERR "building hashes...\n";
while(<IN>){
    if (/^(\d+)\t(.+)\t(.+)$/){
        my ($id, $categ, $title, $goodSpecs) = ($1,$2,$3,0);
        while($_=<IN> and $_!~/^\s*$/){
        	/\t(\S+)\t(.+)\s*$/;
        	my ($tag,$words) = ($1,$2);
        	foreach my $word (split(/\s+/,$words)){
        		$wordtagfreq{$word}{$tag}++;
        	}
        }
    }
}

#check ambig num
my ($n,$ambig) = (0,0);
foreach my $w (keys %wordtagfreq){
	my @t = keys %{$wordtagfreq{$w}};
	if (@t > 1){
		$ambig++;
	}
	$n++;
}
print STDERR "ambig out of train voc = $ambig/$n = " . sprintf("%.2f%", $ambig/$n*100) . "\n";
print STDERR "lte=" , join(" ",map {"$_ ".$wordtagfreq{"lte"}{$_}} keys %{$wordtagfreq{"lte"}}), "\n";

seek IN, 0, 0;
print STDERR "formatting...\n";
while(<IN>){
    if (/^(\d+)\t(.+)\t(.+)$/){
        $numItems++;
        my ($id, $categ, $title, $goodSpecs) = ($1,$2,$3,0);
        my %wordtag = ();
        my @title = split(/\s+/,$title);
        
        my %wordtag = ();
        while($_=<IN> and $_!~/^\s*$/){
            /\t(\S+)\t(.+)\s*$/;
            my ($tag,$words) = ($1,$2);
            foreach my $word (split(/\s+/,$words)){
                $wordtag{$word} = $tag;
            }
        }
        
        my $toPrint = "";
        my $numT = 0;
        foreach my $word (@title){
        	if (exists $xWords{$word}){
        		$toPrint .= "$word " . $xWords{$word} . "\n";
        		$numT++;
        		next;
        	}
        	elsif (exists $wordtag{$word}){        		
        		$toPrint .= "$word " . $wordtag{$word} . "\n";
        		$numT++;
        		next;
        	}
        	elsif ( exists $wordtagfreq{$word} ){
        		if ( (keys %{$wordtagfreq{$word}} == 1)){
	        		my $tag = (keys %{$wordtagfreq{$word}})[0];
	        		if ($wordtagfreq{$word}{$tag} > 10){
	        			$toPrint .= "$word $tag\n";
	        			$numT++;
	        			next;
	        		}
        		}
        		#else {
	        	#	my @tags = sort {$wordtagfreq{$word}{$b} <=> $wordtagfreq{$word}{$a}} (keys %{$wordtagfreq{$word}});
	        	#	#dominant tag
	        	#	if ($wordtagfreq{$word}{$tags[0]} >= $wordtagfreq{$word}{$tags[1]}*10){
	        	#		$toPrint .= "$word $tags[0]\n";
	            #        $numT++;
	            #        print STDERR "domninat tag for word: $word $tags[0] ", $wordtagfreq{$word}{$tags[0]}, " $tags[1] ", $wordtagfreq{$word}{$tags[1]}, "\n";
	            #        next;
	        	#	}
        		#}
        	}
        	
        	$toPrint .= "$word x\n";        	        	
        }
        #if half are tagged print
        if ($numT*2 >= @title){
        	print "$toPrint\n";
        	$itemsKept++;
        }
    }
}

print STDERR "items=$numItems, kept=$itemsKept\n";
